//
//  main.m
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-28.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADPAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
