//
//  ADPManagedCoupon.h
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-31.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ADPManagedCoupon : NSManagedObject

@property (nonatomic, retain) NSString *activity_tag;
@property (nonatomic, retain) NSString *coupon_code;
@property (nonatomic, retain) NSString *coupon_status;
@property (nonatomic, retain) NSDate *creation_date;
@property (nonatomic, retain) NSDate *expiration_date;
@property (nonatomic, retain) NSDate *redeem_date;
@property (nonatomic, retain) NSString *user1_email;
@property (nonatomic, retain) NSString *user1_nickname;
@property (nonatomic, retain) NSString *user1_profile_image;
@property (nonatomic, retain) NSString *user2_email;
@property (nonatomic, retain) NSString *user2_nickname;
@property (nonatomic, retain) NSString *user2_profile_image;

- (void)updateWithDictionary:(NSDictionary *)couponDict;

@end
