//
//  NSDictionary+NonNilObject.m
//  Adore
//
//  Created by Wang on 2014-04-24.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import "NSDictionary+NonNilObject.h"

@implementation NSDictionary (NonNilObject)

#pragma mark -
#pragma mark - Private methods

- (id)objectOfType:(Class)type forKey:(NSString *)key
{
	id object = [self objectForKey:key];
	if ([object isKindOfClass:type]) {
		return object;
	}
	return nil;
}

#pragma mark -
#pragma mark - Public methods

- (id)notNilObjectForKey:(NSString *)key
{
	id object = [self objectForKey:key];
	if ([object isKindOfClass:[NSNull class]]) {
		return nil;
	}
	return object;
}

- (NSNumber *)numberObjectForKey:(NSString *)key
{
	return [self objectOfType:[NSNumber class] forKey:key];
}

- (NSString *)stringObjectForKey:(NSString *)key
{
	return [self objectOfType:[NSString class] forKey:key];
}

- (NSArray *)arrayObjectForKey:(NSString *)key
{
	return [self objectOfType:[NSArray class] forKey:key];
}

- (NSDictionary *)dictionaryObjectForKey:(NSString *)key
{
	return [self objectOfType:[NSDictionary class] forKey:key];
}

- (NSDate *)dateObjectForKey:(NSString *)key
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return [dateFormatter dateFromString:[self stringObjectForKey:key]];
}

- (NSInteger)integerForKey:(NSString *)key
{
	return [[self objectOfType:[NSNumber class] forKey:key] integerValue];
}

- (BOOL)boolForKey:(NSString *)key
{
	return [[self objectOfType:[NSNumber class] forKey:key] boolValue];
}


- (void)setNoneNilValue:(id)value forKey:(NSString *)key
{
    if (value) {
        [self setValue:value forKey:key];
    }
    else {

    }
}

@end
