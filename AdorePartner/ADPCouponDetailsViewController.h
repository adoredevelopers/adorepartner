//
//  ADPCouponDetailsViewController.h
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-04.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADPCouponDetailsViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UITableView *couponDetailTableView;

@property (nonatomic, strong) NSUserDefaults *defaults;
@property (nonatomic, strong) NSDictionary *coupon;

@end
