//
//  Constants.h
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-29.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kADP_SERVER_HOST = @"http://dev.adoreapp.ca:15522";
static NSString * const kADP_SERVER_LOGIN_URL = @"/login_partner";
static NSString * const kADP_SERVER_PROFILE_IMAGE_DIR = @"/uploads/profile_images";
static NSString * const kADP_REDEEM_COUPON_METHOD = @"/redeem_coupon";

@interface ADPConstants : NSObject

+ (NSDictionary *)getActivityMap;

@end
