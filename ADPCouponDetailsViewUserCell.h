//
//  ADPCouponDetailsViewUserCell.h
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-04.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADPCouponDetailsViewUserCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *userProfileImage;
@property (nonatomic, weak) IBOutlet UILabel *userNicknameLabel;

@end
