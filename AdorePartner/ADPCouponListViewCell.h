//
//  ADPCouponListViewCellTableViewCell.h
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-03.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADPCouponListViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *activity_image_view;
@property (nonatomic, weak) IBOutlet UILabel *activity_type_label;
@property (nonatomic, weak) IBOutlet UILabel *redeem_date_label;

@end
