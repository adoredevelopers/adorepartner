//
//  Helper.m
//  AdorePartner
//
//  Created by Chao Lu on 2015-03-04.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import "ADPHelper.h"
#import <CommonCrypto/CommonDigest.h>

@implementation ADPHelper

+ (NSData *)sha256:(NSData *)data {
    unsigned char hash[CC_SHA256_DIGEST_LENGTH];
    if ( CC_SHA256([data bytes], (CC_LONG)[data length], hash) ) {
        NSData *sha256 = [NSData dataWithBytes:hash length:CC_SHA256_DIGEST_LENGTH];
        return sha256;
    }
    return nil;
}

@end
