//
//  ADPCouponDetailsViewQRCell.h
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-06.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADPCouponDetailsViewQRCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *couponQRCodeImageView;

@end
