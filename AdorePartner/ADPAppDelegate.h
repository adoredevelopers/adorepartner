//
//  AppDelegate.h
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-28.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

