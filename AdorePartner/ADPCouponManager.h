//
//  ADPCouponManager.h
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-31.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface ADPCouponManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (ADPCouponManager *)sharedInstance;
- (void)removeAllRecordsForEntity:(NSString *)entityName;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
