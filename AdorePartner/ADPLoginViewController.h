//
//  ADPLoginViewController.h
//  AdorePartner
//
//  Created by Chao Lu on 2015-02-20.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BButton.h"

@protocol loginSuccessDelegate <NSObject>
@required
- (void) loginViewControllerDismissedWithSuccessResult:(NSDictionary *)loginResultData;
@end

@interface ADPLoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *logoAvatarImage;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet BButton *loginBtn;

@property (strong, nonatomic) id<loginSuccessDelegate> delegate;

- (IBAction)loginClick:(id)sender;
- (IBAction)backgroundClick:(id)sender;

@end
