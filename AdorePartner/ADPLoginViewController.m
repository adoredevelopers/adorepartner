//
//  ADPLoginViewController.m
//  AdorePartner
//
//  Created by Chao Lu on 2015-02-20.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import "ADPLoginViewController.h"
#import "ADPHelper.h"
#import "ADPConstants.h"
#import <NSHash/NSString+NSHash.h>

@interface ADPLoginViewController ()

@end

@implementation ADPLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"adore_partner_bg"]];
    backgroundView.frame = self.view.frame;
    backgroundView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:backgroundView];
    [self.view sendSubviewToBack:backgroundView];
    
    self.logoAvatarImage.layer.cornerRadius = 4;
    self.logoAvatarImage.clipsToBounds = YES;
    [self.loginBtn setStyle:BButtonStyleBootstrapV3];
    [self.loginBtn setType:BButtonTypeGray];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginClick:(id)sender {
    NSInteger success = 0;
    @try {
        if([[self.email text] isEqualToString:@""]) {
            [self alert:@"Please enter Email and Password" withTitle:@"Sign in Failed!" andTag:0];
        } else {
            NSString *password_hash = [[self.password text] SHA256];
            NSLog(@"%@", password_hash);
            NSString *post =[[NSString alloc] initWithFormat:@"email=%@&password_hash=%@",[self.email text],password_hash];
            NSLog(@"PostData: %@", post);
            
            NSString *loginUrlString = [NSString stringWithFormat:@"%@%@", kADP_SERVER_HOST, kADP_SERVER_LOGIN_URL];
            NSURL *url=[NSURL URLWithString:loginUrlString];
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
            
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            NSLog(@"Response code: %ld", (long)[response statusCode]);
            
            if ([response statusCode] >= 200 && [response statusCode] < 300)
            {
                NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"Response ==> %@", responseData);
                
                NSError *error = nil;
                NSDictionary *jsonData = [NSJSONSerialization
                                          JSONObjectWithData:urlData
                                          options:NSJSONReadingMutableContainers
                                          error:&error];
                
                success = [jsonData[@"success"] integerValue];
                NSLog(@"Success: %ld",(long)success);
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                if(success == 1)
                {
                    NSLog(@"Login SUCCESS");
                    [defaults setBool:YES forKey:@"isLoggedIn"];
                    [defaults synchronize];
                    [self.delegate loginViewControllerDismissedWithSuccessResult:jsonData[@"data"]];
                } else {
                    NSLog(@"Login FAILED");
                    [defaults setBool:NO forKey:@"isLoggedIn"];
                    [defaults synchronize];
                    //NSString *error_msg = (NSString *) jsonData[@"error_message"];
                    [self alert:@"Wrong username or password" withTitle:@"Sign in Failed!" andTag:0];
                }
            } else {
                //if (error) NSLog(@"Error: %@", error);
                [self alert:@"Connection Failed" withTitle:@"Sign in Failed!" andTag:0];
            }
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [self alert:@"Sign in Failed." withTitle:@"Error!" andTag:0];
    }
    if (success) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)alert:(NSString *)msg withTitle:(NSString *)title andTag:(int)tag
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
}

- (IBAction)backgroundClick:(id)sender {
    [self.view endEditing:YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField.returnKeyType == UIReturnKeyNext) {
        UIView *next = [[textField superview] viewWithTag:textField.tag+1];
        [next becomeFirstResponder];
    } else if (textField.returnKeyType == UIReturnKeyGo) {
        [self loginClick:self];
    } else {
        [textField resignFirstResponder];
    }
    return YES;
}

@end
