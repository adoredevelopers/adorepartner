//
//  ADPCouponListViewCellTableViewCell.m
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-03.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import "ADPCouponListViewCell.h"

@implementation ADPCouponListViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
