//
//  ADPManagedCoupon.m
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-31.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import "ADPManagedCoupon.h"
#import "ADPConstants.h"

@implementation ADPManagedCoupon

@dynamic activity_tag;
@dynamic coupon_code;
@dynamic coupon_status;
@dynamic creation_date;
@dynamic expiration_date;
@dynamic redeem_date;
@dynamic user1_email;
@dynamic user1_nickname;
@dynamic user1_profile_image;
@dynamic user2_email;
@dynamic user2_nickname;
@dynamic user2_profile_image;

-(void)updateWithDictionary:(NSDictionary *)couponDict
{
    self.activity_tag = [[ADPConstants getActivityMap] valueForKey:[[couponDict valueForKey:@"activity_tag"] stringValue]];
    self.coupon_code = [couponDict valueForKey:@"coupon_code"];
    self.coupon_status = [couponDict valueForKey:@"coupon_status"];
    
    NSString *creation_date_string = [couponDict valueForKey:@"creation_date"];
    self.creation_date = [self getNSDateFromISODateString:creation_date_string];
    
    NSString *expiration_date_string = [couponDict valueForKey:@"expiration_date"];
    self.expiration_date = [self getNSDateFromISODateString:expiration_date_string];
    
    NSString *redeem_date_string = [couponDict valueForKey:@"redeem_date"];
    self.redeem_date = [self getNSDateFromISODateString:redeem_date_string];
    
    NSDictionary *user1 = [couponDict objectForKey:@"user1"];
    self.user1_email = [user1 valueForKey:@"email"];
    self.user1_nickname = [user1 valueForKey:@"nickname"];
    self.user1_profile_image = [user1 valueForKey:@"profile_image"];
    NSDictionary *user2 = [couponDict objectForKey:@"user2"];
    self.user2_email = [user2 valueForKey:@"email"];
    self.user2_nickname = [user2 valueForKey:@"nickname"];
    self.user2_profile_image = [user2 valueForKey:@"profile_image"];
}

- (NSDate *)getNSDateFromISODateString:(NSString *)dateString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *date = [formatter dateFromString:dateString];
    
    return date;
}

@end
