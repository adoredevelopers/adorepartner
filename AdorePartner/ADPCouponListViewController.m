//
//  ViewController.m
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-28.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import "ADPCouponListViewController.h"
#import "ADPCouponListViewCell.h"
#import "ADPCouponDetailsViewController.h"
#import "QRCodeReaderViewController.h"
#import "ADPLoginViewController.h"
#import "ADPCouponManager.h"
#import "NSDictionary+NonNilObject.h"
#import "ADPConstants.h"
#import "ADPHelper.h"

@interface ADPCouponListViewController ()
{
    NSDictionary *selectedCoupon;
}

@end

@implementation ADPCouponListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _loginResultData = [[NSDictionary alloc]init];
    
    _couponManager = [ADPCouponManager sharedInstance];
    
    [self fetchCouponsFromCoreData];
    
    _defaults = [NSUserDefaults standardUserDefaults];
    BOOL isLoggedIn = [self.defaults boolForKey:@"isLoggedIn"];
    if (!isLoggedIn) {
        // Presenting Login View Modally
        [self performSegueWithIdentifier:@"LoginModalSegue" sender:self];
    } else {
        _token = [self.defaults stringForKey:@"partnerToken"];
    }
    
    // Do any additional setup after loading the view, typically from a nib.
    
    // drop all records in coredata
    
    // populate coredata with loginResultData coupon info
    
    // perform fetch from coredata
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSManagedObjectContext *managedObjectContext = [couponManager managedObjectContext];
//    NSEntityDescription *couponEntity = [NSEntityDescription entityForName:@"Coupon" inManagedObjectContext:managedObjectContext];
//    [fetchRequest setEntity:couponEntity];
//    NSError *error = nil;
//    _couponArray = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
//    if (error) {
//        NSLog(@"Unable to execute fetch request.");
//        NSLog(@"%@, %@", error, error.localizedDescription);
//    } else {
//        NSLog(@"%@", _couponArray);
//    }
}

- (void)fetchCouponsFromCoreData
{
    // Initialize Fetch Request
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Coupon"];
    // Add Sort Descriptors
    [fetchRequest setSortDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"redeem_date" ascending:NO]]];
    // Initialize Fetched Results Controller
    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[self.couponManager managedObjectContext] sectionNameKeyPath:nil cacheName:nil];
    // Configure Fetched Results Controller
    [self.fetchedResultsController setDelegate:self];
    // Perform Fetch
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    if (error) {
        NSLog(@"Unable to perform fetch.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sections = [self.fetchedResultsController sections];
    id<NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ADPCouponListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponListViewCell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setSelectedCouponAtIndexPath:indexPath];
    
    return indexPath;
}

- (void)setSelectedCouponAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *couponRecord = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSArray *keys = [[[couponRecord entity] attributesByName] allKeys];
    selectedCoupon = [couponRecord dictionaryWithValuesForKeys:keys]; //will result in faults
}

- (void)configureCell:(ADPCouponListViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // Fetch Record
    NSManagedObject *couponRecord = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // Update Cell
    cell.activity_type_label.text = [couponRecord valueForKey:@"activity_tag"];
    
    NSDate *redeemDate = [couponRecord valueForKey:@"redeem_date"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSString *redeemDateString = [formatter stringFromDate:redeemDate];
    cell.redeem_date_label.text = redeemDateString;
}

#pragma mark - Button Actions

- (IBAction)scanAction:(id)sender
{
    QRCodeReaderViewController *reader = [QRCodeReaderViewController new];
    reader.modalPresentationStyle = UIModalPresentationFormSheet;
    
    // Using delegate methods
    reader.delegate = self;
    
    // Or by using blocks
    /*[reader setCompletionWithBlock:^(NSString *result) {
        [self dismissViewControllerAnimated:YES completion:^{
            NSString *token = @"PARTNER_TOKEN";
            NSString *couponRedeemUrl = [NSMutableString stringWithFormat:@"%@%@?token=%@&coupon_code=%@", kADP_SERVER_HOST, kADP_REDEEM_COUPON_METHOD, token, result];
            NSLog(@"%@", couponRedeemUrl);
        }];
    }];*/
    
    [self presentViewController:reader animated:YES completion:NULL];
}

#pragma - LoginSuccess Delegate Method

- (void)loginViewControllerDismissedWithSuccessResult:(NSDictionary *)loginResultData
{
    [self.couponManager removeAllRecordsForEntity:@"Coupon"];
    
    _loginResultData = loginResultData;
    _token = self.loginResultData[@"partner"][@"token"];
    NSString *couponDiscountMessage = self.loginResultData[@"partner"][@"coupon_discount_message"];
    [self.defaults setValue:self.token forKey:@"partnerToken"];
    [self.defaults setValue:couponDiscountMessage forKey:@"couponDiscountMessage"];
    NSLog(@"%@", self.loginResultData);
    NSLog(@"%@", self.token);
    for (NSDictionary *coupon in self.loginResultData[@"coupons"]) {
        NSMutableDictionary *preparedCoupon = [NSMutableDictionary dictionaryWithDictionary:coupon];
        [preparedCoupon removeObjectForKey:@"coupon_owners"];
        NSArray *couponOwners = [coupon arrayObjectForKey:@"coupon_owners"];
        for (NSDictionary *user in self.loginResultData[@"users"]) {
            if ([user[@"email"] isEqualToString:couponOwners[0]] || [user[@"email"] isEqualToString:couponOwners[1]]) {
                NSDictionary *couponOwner = @{
                                              @"email": user[@"email"],
                                              @"nickname": user[@"profile"][@"nickname"],
                                              @"profile_image": [user[@"display_image_urls"] firstObject] ? [user[@"display_image_urls"] firstObject] : @""
                                              };
                if ([preparedCoupon objectForKey:@"user1"] == nil) {
                    [preparedCoupon setObject:couponOwner forKey:@"user1"];
                } else {
                    [preparedCoupon setObject:couponOwner forKey:@"user2"];
                    break;
                }
            }
        }
        [self recordCouponWithDictionary:preparedCoupon];
    }
    [self.tableView reloadData];
}

#pragma mark - QRCodeReader Delegate Methods

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result
{
    [self dismissViewControllerAnimated:YES completion:^{
        NSString *couponRedeemUrl = [NSMutableString stringWithFormat:@"%@%@?token=%@&coupon_code=%@", kADP_SERVER_HOST, kADP_REDEEM_COUPON_METHOD, self.token, result];
        NSLog(@"%@", couponRedeemUrl);
        [self redeemCouponUsingUrl:couponRedeemUrl];
    }];
}

- (void)readerDidCancel:(QRCodeReaderViewController *)reader
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSUInteger) DeviceSystemMajorVersion
{
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSString *systemVersion = [UIDevice currentDevice].systemVersion;
        _deviceSystemMajorVersion = [[systemVersion componentsSeparatedByString:@"."][0] intValue];
    });
    
    return _deviceSystemMajorVersion;
}

#pragma Redeem Coupon URL Connection

- (void)redeemCouponUsingUrl:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    
    NSURLConnection *result = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    NSLog(@"%@", result);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Unable to fetch data");
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *err;
    _responseJSON = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:&err];
    NSLog(@"%@", _responseJSON);
    
    NSString *jsonDictString = [NSString stringWithFormat:@"%@", _responseJSON];
    NSLog(@"%@", jsonDictString);
    
    BOOL success = [[_responseJSON valueForKey:@"success"] integerValue];
    if (success)
    {
        [self recordCouponWithDictionary:[_responseJSON objectForKey:@"data"]];
        [self.tableView reloadData];
        
        NSIndexPath *zeroIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self setSelectedCouponAtIndexPath:zeroIndexPath];
        [self performSegueWithIdentifier:@"CouponDetailsPushSegue" sender:self];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Response" message:jsonDictString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"CouponDetailsPushSegue"]) {
        ADPCouponDetailsViewController *couponDetailsVC = (ADPCouponDetailsViewController *)[[segue.destinationViewController viewControllers] firstObject];
        [couponDetailsVC setCoupon:selectedCoupon];
    }
    if ([segue.identifier isEqualToString:@"LoginModalSegue"]) {
        ADPLoginViewController *loginVC = (ADPLoginViewController *)segue.destinationViewController;
        loginVC.delegate = self;
    }
}

#pragma Core Data

- (void)recordCouponWithDictionary:(NSDictionary *)couponDict
{
    // Create Managed Object
    ADPCouponManager *couponManager = [ADPCouponManager sharedInstance];
    NSManagedObjectContext *managedObjectContext = [couponManager managedObjectContext];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Coupon" inManagedObjectContext:managedObjectContext];
    ADPManagedCoupon *newCoupon = [[ADPManagedCoupon alloc] initWithEntity:entityDescription insertIntoManagedObjectContext:managedObjectContext];
    [newCoupon updateWithDictionary:couponDict];
    
    NSError *error;
    if (![newCoupon.managedObjectContext save:&error]) {
        NSLog(@"Unable to save managed object context.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    switch (type) {
        case NSFetchedResultsChangeInsert: {
            NSIndexPath *zeroIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:zeroIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeDelete: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
        case NSFetchedResultsChangeUpdate: {
            [self configureCell:(ADPCouponListViewCell *)[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
        }
        case NSFetchedResultsChangeMove: {
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        }
    }
}

// Partner Logout

- (IBAction)logoutClick:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"isLoggedIn"];
    [defaults removeObjectForKey:@"partnerToken"];
    [defaults removeObjectForKey:@"couponDiscountMessage"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"LoginModalSegue" sender:self];
}

@end
