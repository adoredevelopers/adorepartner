//
//  ADPCouponDetailsViewController.m
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-04.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ADPCouponDetailsViewController.h"
#import "ADPCouponDetailsViewQRCell.h"
#import "ADPCouponDetailsViewCouponCell.h"
#import "ADPCouponDetailsViewUserCell.h"
#import "NSString+ThumbnailURL.h"
#import "ADPConstants.h"

@interface ADPCouponDetailsViewController ()

@end

@implementation ADPCouponDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _defaults = [NSUserDefaults standardUserDefaults];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Customers";
            break;
        case 1:
            return @"Coupon";
        default:
            return nil;
            break;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 2;
            break;
        case 1:
            return 5;
        default:
            return 0;
            break;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        ADPCouponDetailsViewUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponDetailsViewUserCell" forIndexPath:indexPath];
        if (indexPath.row == 0) {
            cell.userNicknameLabel.text = [self.coupon valueForKey:@"user1_nickname"];
            NSString *imageUrlString = [[NSString stringWithFormat:@"%@%@/%@", kADP_SERVER_HOST, kADP_SERVER_PROFILE_IMAGE_DIR, [self.coupon valueForKey:@"user1_profile_image"]] thumbnailImageUrl160];
            NSLog(@"%@", imageUrlString);
            NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
            [cell.userProfileImage sd_setImageWithURL:imageUrl];
        }
        if (indexPath.row == 1) {
            cell.userNicknameLabel.text = [self.coupon valueForKey:@"user2_nickname"];
            NSString *imageUrlString = [[NSString stringWithFormat:@"%@%@/%@", kADP_SERVER_HOST, kADP_SERVER_PROFILE_IMAGE_DIR, [self.coupon valueForKey:@"user2_profile_image"]] thumbnailImageUrl160];
            NSLog(@"%@", imageUrlString);
            NSURL *imageUrl = [NSURL URLWithString:imageUrlString];
            [cell.userProfileImage sd_setImageWithURL:imageUrl];
        }
        return cell;
    } else if (indexPath.section == 1) {
        ADPCouponDetailsViewCouponCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CouponDetailsViewCouponCell" forIndexPath:indexPath];
        if (indexPath.row == 0) {
            cell.nameLabel.text = @"Coupon Code:";
            cell.valueLabel.text = [self.coupon valueForKey:@"coupon_code"];
        }
        if (indexPath.row == 1) {
            cell.nameLabel.text = @"Discount:";
            cell.valueLabel.text = [self.defaults valueForKey:@"couponDiscountMessage"];
        }
        if (indexPath.row == 2) {
            cell.nameLabel.text = @"Activity Tag:";
            cell.valueLabel.text = [self.coupon valueForKey:@"activity_tag"];
        }
        if (indexPath.row == 3) {
            NSDate *creationDate = [self.coupon valueForKey:@"creation_date"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            //Optionally for time zone conversions
            [formatter setTimeZone:[NSTimeZone systemTimeZone]];
            NSString *creationDateString = [formatter stringFromDate:creationDate];
            
            cell.nameLabel.text = @"Creation Date:";
            cell.valueLabel.text = creationDateString;
        }
        if (indexPath.row == 4) {
            NSDate *redeemDate = [self.coupon valueForKey:@"redeem_date"];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            //Optionally for time zone conversions
            [formatter setTimeZone:[NSTimeZone systemTimeZone]];
            NSString *redeemDateString = [formatter stringFromDate:redeemDate];
            
            cell.nameLabel.text = @"Redeem Date:";
            cell.valueLabel.text = redeemDateString;
        }
        return cell;
    } else {
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 89;
    } else {
        return 44;
    }
}

-(UIImage *)generateQRCodeImageFromString:(NSString *)couponCodeString
{
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    
    NSLog(@"filterAttributes:%@", filter.attributes);
    
    [filter setDefaults];
    
    NSData *data = [couponCodeString dataUsingEncoding:NSUTF8StringEncoding];
    [filter setValue:data forKey:@"inputMessage"];
    
    CIImage *outputImage = [filter outputImage];
    
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef cgImage = [context createCGImage:outputImage
                                       fromRect:[outputImage extent]];
    
    UIImage *qrCodeImage = [UIImage imageWithCGImage:cgImage
                                         scale:1.
                                   orientation:UIImageOrientationUp];
    
    // Resize without interpolating
    UIImage *resizedQRCodeImage = [self resizeImage:qrCodeImage
                             withQuality:kCGInterpolationNone
                                    rate:5.0];
    
    CGImageRelease(cgImage);
    
    return resizedQRCodeImage;
}

- (UIImage *)resizeImage:(UIImage *)image withQuality:(CGInterpolationQuality)quality rate:(CGFloat)rate
{
    UIImage *resized = nil;
    CGFloat width = image.size.width * rate;
    CGFloat height = image.size.height * rate;
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, quality);
    [image drawInRect:CGRectMake(0, 0, width, height)];
    resized = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resized;
}

- (CIImage *)createQRCodeImageFromString:(NSString *)couponCodeString
{
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [couponCodeString dataUsingEncoding:NSUTF8StringEncoding];
    
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"M" forKey:@"inputCorrectionLevel"];
    
    // Send the image back
    return qrFilter.outputImage;
}

- (UIImage *)createNonInterpolatedUIImageFromCIImage:(CIImage *)image withScale:(CGFloat)scale
{
    // Render the CIImage into a CGImage
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:image fromRect:image.extent];
    
    // Now we'll rescale using CoreGraphics
    UIGraphicsBeginImageContext(CGSizeMake(image.extent.size.width * scale, image.extent.size.width * scale));
    CGContextRef context = UIGraphicsGetCurrentContext();
    // We don't want to interpolate (since we've got a pixel-correct image)
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    // Get the image out
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    // Tidy up
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    
    return scaledImage;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
