//
//  Constants.m
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-29.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import "ADPConstants.h"

@implementation ADPConstants

+ (NSDictionary *)getActivityMap
{
    static NSDictionary *activityMap = nil;
    
    if (activityMap == nil)
    {
        activityMap = @{
                        @"1": @"tea",
                        @"2": @"coffee",
                        @"3": @"board games",
                        @"4": @"food",
                        @"5": @"drinks",
                        @"6": @"froyo",
                        @"7": @"bowling",
                        @"8": @"dart",
                        @"9": @"go kart",
                        @"10": @"mini golf/putting",
                        @"11": @"karaoke"
                        };
    }
    return activityMap;
}

@end
