//
//  ADPCouponDetailsViewCouponCell.h
//  AdorePartner
//
//  Created by Chao Lu on 2015-01-04.
//  Copyright (c) 2015 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADPCouponDetailsViewCouponCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;

@end
