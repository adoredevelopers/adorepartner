//
//  ViewController.h
//  AdorePartner
//
//  Created by Chao Lu on 2014-12-28.
//  Copyright (c) 2014 Hoolusoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "QRCodeReaderViewController.h"
#import "ADPCouponManager.h"
#import "ADPManagedCoupon.h"
#import "ADPLoginViewController.h"

@interface ADPCouponListViewController : UITableViewController <NSFetchedResultsControllerDelegate, QRCodeReaderDelegate, loginSuccessDelegate>

@property (nonatomic, strong) NSUserDefaults *defaults;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSArray *couponArray;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSDictionary *responseJSON;
@property (nonatomic, strong) NSDictionary *loginResultData;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) ADPCouponManager *couponManager;

- (IBAction)logoutClick:(id)sender;

@end

